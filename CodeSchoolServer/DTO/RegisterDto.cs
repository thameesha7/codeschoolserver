﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.DTO
{
    public class RegisterDto
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string SchoolId { get; set; }
        public string AccountType { get; set; }
        public string Grade { get; set; }
        public string ParentName { get; set; }
        public string ParentPhoneNo { get; set; }
        public string Status { get; set; }
    }
}
