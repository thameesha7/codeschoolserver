﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.DTO
{
    public class DevicesDto
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string GroupId { get; set; }
        public string DeviceName { get; set; }
        public int DeviceAccountType { get; set; }
        public string DeviceFamily { get; set; }
        public string BoardType { get; set; }
        public string Availability { get; set; }
        public string Status { get; set; }
        public string LastProject { get; set; }
        public string LastSoftwareVersion { get; set; }
        public bool IsDefault { get; set; }
        public string WifiSsid { get; set; }
        public string WifiPassword { get; set; }
    }
}
