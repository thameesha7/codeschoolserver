﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.DTO
{
    public class GroupDto
    {
        [Key]
        [StringLength(200)]
        public string Id { get; set; }
        [Required]
        [StringLength(200)]
        public string SchoolId { get; set; }
        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }
        [StringLength(50)]
        public string GroupDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
}
