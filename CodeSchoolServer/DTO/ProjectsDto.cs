﻿using CodeSchoolServer.Enum;
using CodeSchoolServer.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.DTO
{
    public class ProjectsDto
    {
        [Key]
        [StringLength(200)]
        public string Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProjectType { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd'T'HH:mm:ss.zzz}")]
        //[TypeConverter(typeof(DateTimeKind))]
        public DateTime CreateDate { get; set; } = DateTime.UtcNow;
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        [StringLength(200)]
        public string UserId { get; set; }
        [StringLength(200)]
        public string GroupId { get; set; }
        public string Status { get; set; }
        public string ProjectUrl { get; set; }
    }
}
