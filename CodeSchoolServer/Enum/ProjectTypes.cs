﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.Enum
{
    public enum  ProjectTypes
    {
        Personal = 1,
        Group = 2
    }
}
