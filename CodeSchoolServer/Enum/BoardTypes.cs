﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.Enum
{
    public enum BoardTypes
    {
        [Display(Name="Uno",ShortName = "arduino:avr:uno")]
        uno = 1,
        [Display(Name = "NodeMCU", ShortName = "esp8266:esp8266:nodemcu")]
        NodeMCU = 2
    }
}
