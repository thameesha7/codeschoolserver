﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeSchoolServer.Models
{
    public partial class Schools
    {
        public Schools()
        {
            Groups = new HashSet<Groups>();
            UserProfile = new HashSet<UserProfile>();
        }

        [Key]
        public string Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Code { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public int PhoneNumber { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        [InverseProperty("School")]
        public virtual ICollection<Groups> Groups { get; set; }
        [InverseProperty("School")]
        public virtual ICollection<UserProfile> UserProfile { get; set; }
    }
}
