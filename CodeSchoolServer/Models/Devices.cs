﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeSchoolServer.Models
{
    public partial class Devices
    {
        [Key]
        [StringLength(200)]
        public string Id { get; set; }
        [StringLength(450)]
        public string UserId { get; set; }
        [StringLength(450)]
        public string GroupId { get; set; }
        [StringLength(200)]
        public string DeviceName { get; set; }
        public int DeviceAccountType { get; set; }
        [StringLength(200)]
        public string DeviceFamily { get; set; }
        public string BoardType { get; set; }
        [StringLength(200)]
        public string Availability { get; set; }
        [StringLength(200)]
        public string Status { get; set; }
        [StringLength(200)]
        public string LastProject { get; set; }
        [StringLength(200)]
        public string LastSoftwareVersion { get; set; }
        public bool IsDefault { get; set; }
        [Required]
        [Column("WifiSSID")]
        [StringLength(200)]
        public string WifiSsid { get; set; }
        [Required]
        [StringLength(200)]
        public string WifiPassword { get; set; }
    }
}
