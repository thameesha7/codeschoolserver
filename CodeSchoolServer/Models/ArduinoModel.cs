﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.Models
{
    public class ArduinoModel
    {
        public string ProjectName { get; set; }
        public string RemoteDeviceId { get; set; }
        public int BoardType { get; set; }
        public string FormType { get; set; }
        public string SourceCode { get; set; }
        public List<IFormFile> SourceFile { get; set; }
        public Stream CompileFile { get; set; }
    }
}
