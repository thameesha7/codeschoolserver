﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeSchoolServer.Models
{
    public partial class Groups
    {
        [Key]
        public string Id { get; set; }
        [Required]
        [StringLength(450)]
        public string SchoolId { get; set; }
        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }
        [StringLength(50)]
        public string GroupDescription { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        [ForeignKey(nameof(SchoolId))]
        [InverseProperty(nameof(Schools.Groups))]
        public virtual Schools School { get; set; }
    }
}
