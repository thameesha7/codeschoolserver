﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeSchoolServer.Models
{
    public partial class UserProfile
    {
        [Key]
        public string Id { get; set; }
        [Required]
        [StringLength(450)]
        public string UserId { get; set; }
        [Required]
        [StringLength(450)]
        public string SchoolId { get; set; }
        [StringLength(200)]
        public string FirstName { get; set; }
        [StringLength(200)]
        public string SecondName { get; set; }
        [StringLength(100)]
        public string AccountType { get; set; }
        [StringLength(200)]
        public string Grade { get; set; }
        [StringLength(450)]
        public string ParentName { get; set; }
        [StringLength(200)]
        public string ParentPhone { get; set; }

        [ForeignKey(nameof(SchoolId))]
        [InverseProperty(nameof(Schools.UserProfile))]
        public virtual Schools School { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(AspNetUsers.UserProfile))]
        public virtual AspNetUsers User { get; set; }
    }
}
