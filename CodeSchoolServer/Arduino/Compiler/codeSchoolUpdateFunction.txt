﻿void checkCodeSchoolUpdates() {
  String softwareVersion = "1";//softwareVersion
  String deviceId = WiFi.macAddress();
  String serverAddress = "http://codeschool.ddns.net/";
  codeSchoolUpdateFreq = codeSchoolUpdateFreq - 1;
  if (codeSchoolUpdateFreq == 0) {
    String getLink = serverAddress + "api/ArduinoUpdate/checkUpdate?deviceId=" + deviceId + "&version=" + softwareVersion;
    HTTPClient http;
    http.begin(getLink);
    int httpCode = http.GET();
    if (httpCode == 200)
    {
      DynamicJsonDocument doc(2048);
      deserializeJson(doc, http.getStream());
      if (String(doc["Status"].as<char*>()).equals("true")) {
        Serial.printf(doc["File"].as<char*>());
        t_httpUpdate_return ret = ESPhttpUpdate.update(doc["File"].as<char*>()); //Location of your binary file
        switch (ret) {
          case HTTP_UPDATE_FAILED:
            Serial.printf("\n HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
            break;
          case HTTP_UPDATE_NO_UPDATES:
            Serial.println("HTTP_UPDATE_NO_UPDATES");
            break;
          case HTTP_UPDATE_OK:
            Serial.println("HTTP_UPDATE_OK");
            break;
        }
      }
    }
    else if (httpCode > 0)
    {
      String payload = http.getString();
      Serial.println(httpCode);
      Serial.println(payload);
    }
    else {
      Serial.println("Error on HTTP request");
    }
    http.end();
    codeSchoolUpdateFreq = 5000;
  }
}