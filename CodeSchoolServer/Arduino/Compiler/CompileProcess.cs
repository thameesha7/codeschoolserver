﻿using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodeSchoolServer.Arduino.Compiler
{
    public class CompileProcess
    {
        private readonly IHostingEnvironment _hostingEnvironment = null;
        private readonly ApplicationDbContext _context;
        private string processorPath = Environment.CurrentDirectory + @"\Arduino\Compiler\";
        private string projectPath = Environment.CurrentDirectory + @"\Arduino\Projects\";
        public static string mainPath = Environment.CurrentDirectory + @"\Arduino\Projects\";

        public CompileProcess()
        {
            
        }

        public CompileProcess(ApplicationDbContext applicationDbContext)
        {
            _context = applicationDbContext;
        }

        public CompileProcess(IHostingEnvironment HostingEnv)
        {
            _hostingEnvironment = HostingEnv;
        }

        #region webFunction
        public string Compile(string clientId, string projectName, string boardType = "esp8266:esp8266:nodemcu")
        {
            try
            {
                string sourceCodePath = "\"" + Environment.CurrentDirectory + @"\Arduino\Projects\" + clientId + @"\" + projectName + ".ino" + "\"";
                string sourceBuildPath = "\"" + Environment.CurrentDirectory + @"\Arduino\Projects\" + clientId + @"\" + "BuildPath" + "\"";
                string sourceOutPath = "\"" + Environment.CurrentDirectory + @"\Arduino\Projects\" + clientId + @"\" + projectName + ".bin" + "\"";
                string argumentTemplate = "arduino-cli.exe compile -b " + boardType + " " + sourceCodePath + " --build-path " + sourceBuildPath + " -o " + sourceOutPath;

                using (Process compiler = new Process())
                {
                    compiler.StartInfo.FileName = "cmd.exe";
                    compiler.StartInfo.WorkingDirectory = processorPath;
                    compiler.StartInfo.RedirectStandardInput = true;
                    compiler.StartInfo.RedirectStandardOutput = true;
                    compiler.StartInfo.CreateNoWindow = false;
                    compiler.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                    compiler.StartInfo.UseShellExecute = false;
                    compiler.StartInfo.UserName = Environment.UserName;
                    compiler.Start();

                    compiler.StandardInput.WriteLine(argumentTemplate);
                    compiler.StandardInput.Flush();
                    compiler.StandardInput.Close();

                    string output = compiler.StandardOutput.ReadToEnd();
                    Console.WriteLine(output);

                    compiler.WaitForExit();
                }
                return sourceOutPath;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }



        }
        public bool SaveSourceCodeByUser(string projectName, string clientName, Stream sourceFile = null, string sourceCode = null)
        {
            string projectSavePath = projectPath + clientName;
            string projectPathName = projectPath + clientName + @"\" + projectName + ".ino";

            if (!Directory.Exists(projectSavePath))
            {
                Directory.CreateDirectory(projectSavePath);
            }

            if (sourceFile != null)
            {

                StreamReader reader = new StreamReader(sourceFile);
                string source = reader.ReadToEnd();
                byte[] byteArray = Encoding.UTF8.GetBytes(source);
                MemoryStream stream = new MemoryStream(byteArray);
                var fileStream = new FileStream(projectPathName, FileMode.Create, FileAccess.Write);
                stream.CopyTo(fileStream);
                fileStream.Dispose();

                return true;
            }
            else if (sourceCode != null)
            {
                // convert string to stream
                byte[] byteArray = Encoding.UTF8.GetBytes(sourceCode);
                //byte[] byteArray = Encoding.ASCII.GetBytes(contents);
                MemoryStream stream = new MemoryStream(byteArray);
                var fileStream = new FileStream(projectPathName, FileMode.Create, FileAccess.Write);
                stream.CopyTo(fileStream);
                fileStream.Dispose();

                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        public string Compile(ProjectsDto projectsDto, string boardType = "esp8266:esp8266:nodemcu")
        {
            string sourceCodePath = "";
            string sourceBuildPath = "";
            string cacheBuildPath = "";
            string sourceOutPath = "";
            string argumentTemplate = "";

            try
            {
                if(projectsDto.ProjectType == 1)
                {
                    sourceCodePath = "\"" + mainPath + @"\Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".ino" + "\"";
                    sourceBuildPath = "\"" + mainPath + @"\Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + "BuildPath" + "\"";
                    cacheBuildPath = "\"" + mainPath + @"\Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + "cacheBuildPath" + "\"";
                    sourceOutPath = "\"" + mainPath + @"\Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".bin" + "\"";
                }
                else if (projectsDto.ProjectType == 2)
                {
                    sourceCodePath = "\"" + mainPath + @"\Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".ino" + "\"";
                    sourceBuildPath = "\"" + mainPath + @"\Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + "BuildPath" + "\"";
                    cacheBuildPath = "\"" + mainPath + @"\Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + "cacheBuildPath" + "\"";
                    sourceOutPath = "\"" + mainPath + @"\Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".bin" + "\"";
                }

                argumentTemplate = "arduino-cli compile -b " + boardType + " " + sourceCodePath + " --build-path " + sourceBuildPath + " --build-cache-path " + cacheBuildPath + " -o " + sourceOutPath;

                string argumentTemplate2 = "arduino-cli.exe compile -b " + boardType + " " + sourceCodePath + " --build-path " + sourceBuildPath + " --build-cache-path " + cacheBuildPath + " -o " + sourceOutPath;


                ProcessStartInfo compiler;
                compiler = new ProcessStartInfo();
                compiler.FileName = "cmd.exe";
                compiler.WorkingDirectory = processorPath;
                compiler.RedirectStandardInput = true;
                compiler.RedirectStandardOutput = true;
                compiler.RedirectStandardError = true;
                compiler.CreateNoWindow = true;
                compiler.WindowStyle = ProcessWindowStyle.Hidden;
                compiler.UseShellExecute = false;
                compiler.Arguments = "/c \"" + argumentTemplate2 + "\"";

                Process process = new Process();
                process.StartInfo = compiler;
                if (process.Start())
                {
                    Console.WriteLine("process started");
                    process.WaitForExit();
                }
                else
                {
                    StreamReader myStreamReader = process.StandardError;
                    Console.WriteLine(myStreamReader.ReadLine());
                }

                //using (Process process = new Process())
                //{
                //    ProcessStartInfo compiler;
                //    compiler = new ProcessStartInfo();
                //    compiler.FileName = "cmd.exe";
                //    compiler.WorkingDirectory = processorPath;
                //    compiler.RedirectStandardInput = true;
                //    compiler.RedirectStandardOutput = true;
                //    compiler.RedirectStandardError = true;
                //    //compiler.CreateNoWindow = true;
                //    compiler.WindowStyle = ProcessWindowStyle.Maximized;
                //    compiler.UseShellExecute = false;
                //    //compiler.UserName = Environment.UserName;
                //    process.StartInfo = compiler;

                //    //process.Start();
                //    if (process.Start())
                //    {
                //        process.BeginOutputReadLine();
                //        process.BeginErrorReadLine();
                //        Console.WriteLine("process started");
                //        //process.StandardInput.WriteLine("arduino-cli.exe -help");
                //        process.StandardInput.Write(argumentTemplate);
                //        process.StandardInput.Flush();
                //        process.StandardInput.Close();
                //        //string output1 = process.StandardOutput.ReadToEnd();
                //        process.WaitForExit();
                //    }
                //    else
                //    {
                //        StreamReader myStreamReader = process.StandardError;
                //        Console.WriteLine(myStreamReader.ReadLine());
                //    }


                //    //string output = process.StandardOutput.ReadToEnd();
                //    //Console.WriteLine(argumentTemplate);


                //}


                return sourceOutPath;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return null;
            }



        }

        public void SaveXML(ProjectsDto projectsDto, string xml)
        {
            string xmlPath = "";
            UnicodeEncoding uniEncoding = new UnicodeEncoding();
            MemoryStream stream = new MemoryStream();

            if (projectsDto.ProjectType == 1)//personal
            {
                xmlPath = projectPath + @"Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".xml";
            }
            else if (projectsDto.ProjectType == 2)//group
            {
                xmlPath = projectPath + @"Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".xml";
            }

            try
            {
                using (stream)
                {
                    var sw = new StreamWriter(stream, uniEncoding);
                    var fileStream = new FileStream(xmlPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    try
                    {
                        sw.Write(xml);
                        sw.Flush();
                        sw.WriteLine();
                        sw.Flush();
                        stream.Seek(0, SeekOrigin.Begin);
                        stream.CopyTo(fileStream);
                    }
                    finally
                    {
                        sw.Dispose();
                        fileStream.Dispose();
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        public void SaveCode(ProjectsDto projectsDto, DevicesDto devicesDto, string sourceCode)
        {
            string codePath = "";
            UnicodeEncoding uniEncoding = new UnicodeEncoding();
            MemoryStream stream = new MemoryStream();

            string modifyCode = InjectServerCode(sourceCode,devicesDto.WifiSsid, devicesDto.WifiPassword,devicesDto.LastSoftwareVersion);

            if (projectsDto.ProjectType == 1)//personal
            {
                codePath = projectPath + @"Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".ino";
            }
            else if (projectsDto.ProjectType == 2)//group
            {
                codePath = projectPath + @"Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".ino";
            }
            if (File.Exists(codePath))
            {
                File.Delete(codePath);
            }
            using (stream)
            {
                var sw = new StreamWriter(stream, Encoding.Default);
                var fileStream = new FileStream(codePath, FileMode.OpenOrCreate, FileAccess.Write);
                try
                {
                    sw.Write(modifyCode);
                    //sw.WriteLine();
                    sw.Flush();
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(fileStream);
                }
                finally
                {
                    sw.Dispose();
                    fileStream.Dispose();
                }
            }
        }

        public string GetXML(ProjectsDto projectsDto)
        {
            string xmlPath = "";
            string xmlData = "";
            if (projectsDto.ProjectType == 1)//personal
            {
                xmlPath = projectPath + @"Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".xml";
            }
            else if (projectsDto.ProjectType == 2)//group
            {
                xmlPath = projectPath + @"Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".xml";
            }

            xmlData = File.ReadAllText(xmlPath);

            return xmlData;
        }

        public static FileStream GetBin(ProjectsDto projectsDto)
        {
            string sourceOutPath = "";

            if (projectsDto.ProjectType == 1)
            {
                sourceOutPath = mainPath + @"Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".bin";
            }
            else if (projectsDto.ProjectType == 2)
            {
                sourceOutPath = mainPath + @"Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\" + projectsDto.Id + ".bin";
            }
            FileStream fileStream = new FileStream(sourceOutPath, FileMode.Open);
            return fileStream;
        }

        private string InjectServerCode(string source,string WIFIssid, string WIFIpwd, string softwareVersion)
        {
            string outSource = "";
            string codeSchoolFunction = File.ReadAllText(processorPath + "codeSchoolUpdateFunction.txt",Encoding.Default);
            if(!source.Contains("#include <ESP8266httpUpdate.h>"))
            {
                outSource += "#include <ESP8266httpUpdate.h> \n";
            }

            if (!source.Contains("#include <ESP8266HTTPClient.h>"))
            {
                outSource += "#include <ESP8266HTTPClient.h> \n";
            }

            if (!source.Contains("#include <ArduinoJson.h>"))
            {
                outSource += "#include <ArduinoJson.h> \n";
            }

            outSource += source;
            string variables = "\n const char* ssid =     \""+WIFIssid+"\";";
            variables += "\n const char* password = \""+WIFIpwd+"\";";
            variables += "\n int codeSchoolUpdateFreq = 5000; \n \n";
            int setupIndex = outSource.IndexOf("void setup()\n{");
            outSource = outSource.Insert(setupIndex, variables);
            string wifiConfig = "\n \n WiFi.begin(ssid, password); \n";
            wifiConfig += "while (WiFi.status() != WL_CONNECTED) { \n";
            wifiConfig += " Serial.print(\".\"); \n";
            wifiConfig += " delay(1000); \n";
            wifiConfig += "} \n";
            setupIndex = outSource.IndexOf("void setup()\n{");
            outSource = outSource.Insert(setupIndex+14, wifiConfig);
            int loopIndex = outSource.IndexOf("void loop()\n{");
            outSource = outSource.Insert(loopIndex+13, "\n checkCodeSchoolUpdates(); \n");
            outSource += "\n\n"+codeSchoolFunction;
            int versionLineEnd = outSource.IndexOf(";//softwareVersion")-1;
            int versionLineStart = outSource.LastIndexOf("softwareVersion = ")+19;
            string versionNumber = outSource.Substring(versionLineStart, versionLineEnd - versionLineStart);
            outSource = outSource.Replace("softwareVersion = \""+int.Parse(versionNumber)+"\";", "softwareVersion = \"" + softwareVersion + "\";");

            return outSource;
        } 

    }
}
