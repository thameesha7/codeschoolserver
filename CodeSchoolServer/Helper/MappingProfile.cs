﻿using AutoMapper;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeSchoolServer.Helper
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Projects, ProjectsDto>();
            CreateMap<ProjectsDto, Projects>();

            CreateMap<Schools, SchoolDto>();
            CreateMap<SchoolDto, Schools>();

            CreateMap<Groups, GroupDto>();
            CreateMap<GroupDto, Groups>();

            CreateMap<UserProfile, RegisterDto>();
            CreateMap<RegisterDto, UserProfile>();

            CreateMap<Devices, DevicesDto>();
            CreateMap<DevicesDto, Devices>();
        }

    }
}
