﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodeSchoolServer.Controllers.API
{
    [Route("api/[controller]")]
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        // Assign the object in the constructor for dependency injection
        public DeviceController(ApplicationDbContext applicationDbContext,
            IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        [HttpPost]
        public DevicesDto CreateDevice([FromBody]DevicesDto devicesDto)
        {
            DevicesDto returnDevicesDto = new DevicesDto();
            var model = _mapper.Map<Devices>(devicesDto);
            try
            {
                var item = _context.Devices.Add(model);
                int state = _context.SaveChanges();
                Console.WriteLine(state);
                returnDevicesDto = _mapper.Map<DevicesDto>(item.Entity);
                returnDevicesDto.Status = "Success";

                return returnDevicesDto;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<DevicesDto> getAll([FromQuery]string userId)
        {
            try
            {
                //private devices
                var itemsPrivate = _context.Devices.Where(a => a.UserId == userId && a.DeviceAccountType == 1).ToList();
                //public devices
                var itemsPublic = _context.Devices.Where(a => a.DeviceAccountType == 2).ToList();

                var items = itemsPrivate;
                items.AddRange(itemsPublic);
                Console.WriteLine(items.Count);
                return _mapper.Map<List<DevicesDto>>(items);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public DevicesDto SetDefault([FromQuery]string userId, [FromQuery]string deviceId)
        {
            DevicesDto returnDevicesDto = new DevicesDto();
            
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {

                try
                {
                    var itemList = _context.Devices.Where(a => a.UserId == userId).ToList();
                    itemList.ForEach(a => a.IsDefault = false);
                    int state = _context.SaveChanges();
                    Console.WriteLine("device is default "+state);

                    var item = _context.Devices.Single(a => a.Id == deviceId);
                    item.IsDefault = true;
                    _context.Devices.Update(item);
                    int stateu = _context.SaveChanges();
                    Console.WriteLine(stateu);

                    returnDevicesDto = _mapper.Map<DevicesDto>(item);
                    returnDevicesDto.Status = "Success";

                    dbContextTransaction.Commit();
                    return returnDevicesDto;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public List<DevicesDto> GetDefault([FromQuery]string userId)
        {
            try
            {
                var items = _context.Devices.Where(a => a.UserId == userId && a.IsDefault).ToList();
                return _mapper.Map<List<DevicesDto>>(items);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}