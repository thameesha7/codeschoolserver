﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeSchoolServer.Controllers.API
{
    [Route("api/[controller]")]
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class SchoolController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public SchoolController(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        [HttpPost]
        public SchoolDto Add([FromBody]SchoolDto schoolDto)
        {
            var model = _mapper.Map<Schools>(schoolDto);
            model.IsActive = true;
            try
            {
                var item = _context.Schools.Add(model);
                int state = _context.SaveChanges();
                Console.WriteLine(state);
                return _mapper.Map<SchoolDto>(item.Entity);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<SchoolDto> getAllActive()
        {
            try
            {
                var items = _context.Schools.Where(a => a.IsActive == true && a.IsDelete != true).ToList();
                Console.WriteLine(items.Count);
                return _mapper.Map<List<SchoolDto>>(items);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<SchoolDto> getAll()
        {
            try
            {
                var items = _context.Schools.ToList();
                Console.WriteLine(items.Count);
                return _mapper.Map<List<SchoolDto>>(items);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<SchoolDto> getByCode([FromQuery]string code)
        {
            List<SchoolDto> itemList = new List<SchoolDto>();
            try
            {
                var items = _context.Schools.Where(a => a.IsActive == true && a.IsDelete != true && a.Code == code).ToList();
                //Console.WriteLine(items.Count);
                itemList = _mapper.Map<List<SchoolDto>>(items);
                return itemList;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
