﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeSchoolServer.Arduino.Compiler;
using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeSchoolServer.Controllers.API
{
    [Route("api/[controller]")]
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class ArduinoUpdateController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _config;
        private string ProjectFolder = Environment.CurrentDirectory + @"\Arduino\Projects\";
        CompileProcess compileProcess;

        public ArduinoUpdateController(ApplicationDbContext applicationDbContext, 
            IMapper mapper,
            IConfiguration config)
        {
            _context = applicationDbContext;
            _mapper = mapper;
            _config = config;
        }

        [HttpGet]
        public JsonResult checkUpdate([FromQuery]string deviceId , [FromQuery]string version)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            string status = "false";
            string binPath = "";
            DevicesDto devicesDto = new DevicesDto();
            ProjectsDto projectsDto = new ProjectsDto();

            string hostType = _config.GetValue<string>("serverHostAddress:HostType");
            string ip = _config.GetValue<string>("serverHostAddress:Ip");
            string port = _config.GetValue<string>("serverHostAddress:Port");

            var device = _context.Devices.SingleOrDefault(a => a.Id == deviceId);
            if (device != null)
            {
                devicesDto = _mapper.Map<DevicesDto>(device);
                if(devicesDto.LastProject != null && (Int32.Parse(version) < Int32.Parse(devicesDto.LastSoftwareVersion)))
                {
                    var model = _context.Projects.First(a => a.Id == devicesDto.LastProject);
                    projectsDto = _mapper.Map<ProjectsDto>(model);
                    binPath = hostType+ip+"/CodeSchoolServer/api/ArduinoUpdate/downloadbin?projectId="+projectsDto.Id;
                    status = "false";
                }
            }
            
            dic.Add("Status", status);
            dic.Add("File", binPath);
            return new JsonResult(dic);
        }

        [HttpGet]
        public FileResult downloadBin([FromQuery]string projectId)
        {
            ProjectsDto projectsDto = new ProjectsDto();

            var model = _context.Projects.First(a => a.Id == projectId);
            projectsDto = _mapper.Map<ProjectsDto>(model);

            return File(CompileProcess.GetBin(projectsDto), "application/octet-stream",projectId+".bin");
        }

        [HttpPost]
        public string deviceUpdate([FromBody]ArduinoUpdateDto projectData)
        {
            string projectId = projectData.projectId;
            string xmlCode = projectData.xmlCode;
            string arduinoCode = projectData.arduinoCode;
            ProjectsDto projectsDto = new ProjectsDto();
            DevicesDto devicesDto = new DevicesDto();
            compileProcess = new CompileProcess();
            var model = _context.Projects.First(a => a.Id == projectId);
            projectsDto = _mapper.Map<ProjectsDto>(model);
            var devmodel = _context.Devices.First(a => (a.UserId == projectsDto.UserId || a.GroupId == projectsDto.GroupId) && a.IsDefault == true);
            devicesDto = _mapper.Map<DevicesDto>(devmodel);

            devicesDto = UpdateDeviceSoftwareVersionNumber(devicesDto, projectsDto);

            compileProcess.SaveXML(projectsDto, xmlCode);
            compileProcess.SaveCode(projectsDto, devicesDto, arduinoCode);
            return compileProcess.Compile(projectsDto);
        }

        private DevicesDto UpdateDeviceSoftwareVersionNumber(DevicesDto devicesDto, ProjectsDto projectsDto)
        {
            DevicesDto dto = new DevicesDto();
            dto = devicesDto;
            try
            {
                if (dto.LastProject != null || dto.LastProject != "")
                {
                    if ((dto.LastSoftwareVersion != null || dto.LastSoftwareVersion != "") && dto.LastProject.Equals(projectsDto.Id))
                    {
                        dto.LastSoftwareVersion = (int.Parse(dto.LastSoftwareVersion) + 1).ToString();
                    }
                    else
                    {
                        dto.LastSoftwareVersion = "1";
                        dto.LastProject = projectsDto.Id;
                    }
                }
                else
                {
                    dto.LastSoftwareVersion = "1";
                    dto.LastProject = projectsDto.Id;
                }

                var model = _mapper.Map<Devices>(dto);

                var local = _context.Set<Devices>().Local.FirstOrDefault(entry => entry.Id.Equals(model.Id));
                if (local != null)
                {
                    _context.Entry(local).State = EntityState.Detached;
                }
                _context.Entry(model).State = EntityState.Modified;
                //_context.Devices.Update(model);
                _context.SaveChanges();
            }
            catch(Exception ex)
            {

            }
            
            return dto;
        }


    }

    public class ArduinoUpdateDto
    {
        public string projectId { get; set; }
        public string xmlCode { get; set; }
        public string arduinoCode { get; set; }
    }
}
