﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeSchoolServer.Controllers.API
{
    [Route("api/[controller]")]
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public GroupController(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        [HttpPost]
        public GroupDto Add([FromBody]GroupDto groupDto)
        {
            var model = _mapper.Map<Groups>(groupDto);
            model.IsActive = true;
            try
            {
                var item = _context.Groups.Add(model);
                int state = _context.SaveChanges();
                Console.WriteLine(state);
                return _mapper.Map<GroupDto>(item.Entity);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<GroupDto> getAllActive()
        {
            try
            {
                var items = _context.Groups.Where(a => a.IsActive == true && a.IsDelete != true).ToList();
                Console.WriteLine(items.Count);
                return _mapper.Map<List<GroupDto>>(items);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<GroupDto> getAll()
        {
            try
            {
                var items = _context.Groups.ToList();
                Console.WriteLine(items.Count);
                return _mapper.Map<List<GroupDto>>(items);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<GroupDto> getBySchoolId([FromQuery]string schoolId)
        {
            List<GroupDto> itemList = new List<GroupDto>();
            try
            {
                var items = _context.Groups.Where(a => a.IsActive == true && a.IsDelete != true && a.SchoolId == schoolId).ToList();
                //Console.WriteLine(items.Count);
                itemList = _mapper.Map<List<GroupDto>>(items);
                return itemList;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
