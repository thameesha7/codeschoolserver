﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Helper;
using CodeSchoolServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeSchoolServer.Controllers.API
{



    [Route("api/[controller]")]
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class UserManageController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IEmailSender _emailSender;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        //public class LoginModel
        //{
        //    public string email { get; set; }
        //    public string password { get; set; }
        //    public bool rememberMe { get; set; }
        //}

        public UserManageController(SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            ApplicationDbContext applicationDbContext,
            IMapper mapper,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = applicationDbContext;
            _mapper = mapper;
            _emailSender = emailSender;
        }

        [HttpPost]
        public LoginDto SignIn([FromBody]LoginDto loginDto)
        {
            //bool RememberMe = true;
            var result = _signInManager.PasswordSignInAsync(loginDto.Email, loginDto.Password, loginDto.RememberMe, lockoutOnFailure: false).Result;
            if (result.Succeeded)
            {
                var item = _context.Users.Where(a => a.Email == loginDto.Email).FirstOrDefault();
                loginDto.Id = item.Id;
                if (!item.EmailConfirmed)
                {
                    loginDto.Status = "RequiredEmailConfirmed";
                }
                else
                {
                    loginDto.Status = "Succeeded";
                }
                return loginDto;
                //return RedirectToAction("blocky","arduino");
                //return LocalRedirect(returnUrl);
            }else if (result.IsLockedOut)
            {
                loginDto.Status = "LockedOut";
                return loginDto;
            }
            //if (result.RequiresTwoFactor)
            //{
            //    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
            //}
            //if (result.IsLockedOut)
            //{
            //    _logger.LogWarning("User account locked out.");
            //    return RedirectToPage("./Lockout");
            //}

            //return RedirectToPage("./Lockout");
            return new LoginDto();
        }

        [HttpPost]
        public async Task<RegisterDto> RegisterAsync([FromBody]RegisterDto registerDto)
        {
            RegisterDto registerDtoReturn = new RegisterDto();
            var user = new IdentityUser { UserName = registerDto.Email, Email = registerDto.Email };
            var result = await _userManager.CreateAsync(user, registerDto.Password);
            if (result.Succeeded)
            {
                //_logger.LogInformation("User created a new account with password.");
                Console.WriteLine(user.Id);
                var userProfile = _mapper.Map<UserProfile>(registerDto);
                userProfile.UserId = user.Id;
                userProfile.Id = Guid.NewGuid().ToString();
                _context.UserProfile.Add(userProfile);
                _context.SaveChanges();
                registerDtoReturn = _mapper.Map<RegisterDto>(userProfile);

                //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                //code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                //var callbackUrl = Url.Page(
                //    "/Account/ConfirmEmail",
                //    pageHandler: null,
                //    values: new { area = "Identity", userId = user.Id, code = code },
                //    protocol: Request.Scheme);
                string code = getCode(userProfile.UserId);

                await _emailSender.SendEmailAsync(registerDto.Email, "Confirm your email",
                    $"Please confirm your account with this code <b>"+code+"</b> here.");

                if (_userManager.Options.SignIn.RequireConfirmedAccount)
                {
                    registerDtoReturn.Status = "RequireConfirmation";
                }
                else
                {
                    registerDtoReturn.Status = "Success";
                }

                return registerDtoReturn;
            }
            else
            {
                registerDtoReturn.Status = result.Errors.ToString();
            }

            return registerDtoReturn;
        }

        [HttpGet]
        public async Task<RegisterDto> ConfirmEmailAsync([FromQuery]string userId, [FromQuery]string code)
        {
            RegisterDto registerDtoReturn = new RegisterDto();

            var user = await _userManager.FindByIdAsync(userId);
            IdentityResult result = new IdentityResult();
            if (validateCode(userId, code))
            {
                var ecode = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                result = await _userManager.ConfirmEmailAsync(user, ecode);
            }
            
            if (result.Succeeded)
            {
                //_logger.LogInformation("User created a new account with password.");
                var userProfile = _context.UserProfile.Single(a => a.UserId == user.Id);
                registerDtoReturn = _mapper.Map<RegisterDto>(userProfile);
                registerDtoReturn.Status = "Success";

                return registerDtoReturn;
            }
            else
            {
                registerDtoReturn.Status = result.Errors.ToString();
            }

            return registerDtoReturn;
        }


        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]JsonElement value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        private string getCode(string userId)
        {
            string code = "";
            string encryptCode = EncryptionHelper.Encrypt(userId);
            code = encryptCode.Substring(0, 6) + "-" + encryptCode.Substring(6, 4);
            return code;
        }

        private bool validateCode(string userId,string userCode)
        {
            string code = "";
            string encryptCode = EncryptionHelper.Encrypt(userId);
            code = encryptCode.Substring(0, 6) + "-" + encryptCode.Substring(6, 4);
            return code.Equals(userCode);
        }
    }
}
