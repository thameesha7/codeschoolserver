﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace CodeSchoolServer.Controllers.API
{
    [Route("api/[controller]")]
    [Route("api/[controller]/[action]/")]
    [ApiController]
    public class ProjectController : ControllerBase
    {

        // Create a field to store the mapper object
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private string ProjectFolder = Environment.CurrentDirectory + @"\Arduino\Projects\";

        // Assign the object in the constructor for dependency injection
        public ProjectController(ApplicationDbContext applicationDbContext,
            IMapper mapper,
            IWebHostEnvironment HostingEnv)
        {
            _context = applicationDbContext;
            _mapper = mapper;
            _hostingEnvironment = HostingEnv;
        }

        [HttpPost]
        public ProjectsDto CreateProject([FromBody]ProjectsDto projectsDto)
        {
            ProjectsDto returnProjectDto = new ProjectsDto();
            var model = _mapper.Map<Projects>(projectsDto);
            model.IsActive = true;
            model.Id = Guid.NewGuid().ToString();
            try
            {
                var item = _context.Projects.Add(model);
                int state = _context.SaveChanges();
                Console.WriteLine(state);
                returnProjectDto = _mapper.Map<ProjectsDto>(item.Entity);
                returnProjectDto.ProjectUrl = "CodeSchoolServer/Arduino/Blocky?projectId="+item.Entity.Id;
                returnProjectDto.Status = "Success";
                if (state > 0)
                {
                    createProjectFolder(returnProjectDto);
                }

                return returnProjectDto;
            }
            catch(Exception ex)
            {
                return null;
            }
            
        }

        [HttpGet]
        public List<ProjectsDto> getAllActive()
        {
            try
            {
                var items = _context.Projects.Where(a=>a.IsActive == true && a.IsDelete != true).ToList();
                Console.WriteLine(items.Count);
                return _mapper.Map<List<ProjectsDto>>(items);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpGet]
        public List<ProjectsDto> getByProjectType([FromQuery]int projectType, [FromQuery]string userId, [FromQuery]string groupId)
        {
            List<ProjectsDto> projects = new List<ProjectsDto>();
            try
            {
                //personal
                if(projectType == 1)
                {
                    var items = _context.Projects.Where(a => a.IsActive == true && a.IsDelete != true && a.ProjectType == 1 && a.UserId == userId).ToList();
                    //Console.WriteLine(items.Count);
                    projects = _mapper.Map<List<ProjectsDto>>(items);
                }
                //group
                else if(projectType == 2)
                {
                    var items = _context.Projects.Where(a => a.IsActive == true && a.IsDelete != true && a.ProjectType == 2 && a.GroupId == groupId).ToList();
                    //Console.WriteLine(items.Count);
                    projects = _mapper.Map<List<ProjectsDto>>(items);
                }
                return projects;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        private string createProjectFolder(ProjectsDto projectsDto)
        {
            string projectPath = "";

            if (projectsDto.ProjectType == 1)//personal
            {
                projectPath = ProjectFolder + @"Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\";
                if (!Directory.Exists(projectPath))
                {
                    Directory.CreateDirectory(projectPath);
                }
                string projectTemplate = projectPath + projectsDto.Id + ".xml";
                //string projectSourceCode = projectPath + projectsDto.Id + ".ino";
                //string projectBuildPath = projectPath + projectsDto.Id + @"BuildPath\";
                //string projectSourceOut = projectPath + projectsDto.Id + ".bin";
                MemoryStream stream = new MemoryStream();
                var fileStream = new FileStream(projectTemplate, FileMode.Create, FileAccess.Write);
                stream.CopyTo(fileStream);
                fileStream.Dispose();

                return projectPath;
            }
            else if(projectsDto.ProjectType == 2)//group
            {
                projectPath = ProjectFolder + @"Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\";
                if (!Directory.Exists(projectPath))
                {
                    Directory.CreateDirectory(projectPath);
                }
                string projectTemplate = projectPath + projectsDto.Id + ".xml";
                MemoryStream stream = new MemoryStream();
                var fileStream = new FileStream(projectTemplate, FileMode.Create, FileAccess.Write);
                stream.CopyTo(fileStream);
                fileStream.Dispose();

                return projectPath;
            }
            return projectPath;
        }

    }
}