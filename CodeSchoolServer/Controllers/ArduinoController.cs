﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CodeSchoolServer.Arduino.Compiler;
using CodeSchoolServer.Data;
using CodeSchoolServer.DTO;
using CodeSchoolServer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CodeSchoolServer.Controllers
{
    public class arduinoBlockModel
    {
        public int projectId { get; set; }
        public string sourceCode { get; set; }
    }


    //[Authorize]
    public class ArduinoController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;
        private string ProjectFolder = Environment.CurrentDirectory + @"\Arduino\Projects\";
        CompileProcess compileProcess;

        public ArduinoController(SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            ApplicationDbContext applicationDbContext,
            IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("FileUpload")]
        public async Task<IActionResult> GetSourceCode(ArduinoModel model)
        {
            CompileProcess compilerProcess = new CompileProcess();

            if (model.SourceFile != null)
            {
                long size = model.SourceFile.Sum(f => f.Length);

                var filePaths = new List<string>();
                foreach (var formFile in model.SourceFile)
                {
                    if (formFile.Length > 0)
                    {
                        // full path to file in temp location
                        var filePath = Path.GetTempFileName(); //we are using Temp file name just for the example. Add your own file path.
                        filePaths.Add(filePath);

                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            //await formFile.CopyToAsync(stream);
                            compilerProcess.SaveSourceCodeByUser(model.ProjectName, User.Identity.Name, stream);
                        }
                    }
                }
            }
            else
            {
                compilerProcess.SaveSourceCodeByUser(model.ProjectName, User.Identity.Name, null,model.SourceCode);
            }
            

            // process uploaded files
            // Don't rely on or trust the FileName property without validation.

            return View("Index");
        }

        //[HttpPost("FileUpload")]
        //public async Task<IActionResult> FileUpload(List<IFormFile> files)
        //{
        //    long size = files.Sum(f => f.Length);

        //    var filePaths = new List<string>();
        //    foreach (var formFile in files)
        //    {
        //        if (formFile.Length > 0)
        //        {
        //            // full path to file in temp location
        //            var filePath = Path.GetTempFileName(); //we are using Temp file name just for the example. Add your own file path.
        //            filePaths.Add(filePath);

        //            using (var stream = new FileStream(filePath, FileMode.Create))
        //            {
        //                await formFile.CopyToAsync(stream);
        //            }
        //        }
        //    }

        //    // process uploaded files
        //    // Don't rely on or trust the FileName property without validation.

        //    return Ok(new { count = files.Count, size, filePaths });
        //}

        public IActionResult Blocky([FromQuery]string projectId)
        {
            ProjectsDto projectsDto = new ProjectsDto();

            if (Request.Headers.ContainsKey("Authorization"))
            {
                string authValue = Request.Headers["Authorization"].ToString();
                if (authValue != null)
                {
                    byte[] data = Convert.FromBase64String(authValue.Split(null)[1]);
                    string decodedString = Encoding.UTF8.GetString(data);

                    string userEmail = decodedString.Split(':')[0];
                    string password = decodedString.Split(':')[1];

                    var result = _signInManager.PasswordSignInAsync(userEmail, password, false, lockoutOnFailure: false).Result;
                    if (result.Succeeded)
                    {
                        
                        var model = _context.Projects.First(a => a.Id == projectId);
                        projectsDto = _mapper.Map<ProjectsDto>(model);
                        ViewData["ProjectId"] = projectId;
                        getProjectPath(projectsDto);
                        
                        return View();
                    }
                    else
                    {
                        return null;
                    }
                }
            }


            var model2 = _context.Projects.First(a => a.Id == projectId);
            projectsDto = _mapper.Map<ProjectsDto>(model2);
            ViewData["ProjectId"] = projectId;
            getProjectPath(projectsDto);

            return View();

        }

        [HttpPost]
        public void saveProject(string projectId, string xmlCode)
        {
            ProjectsDto projectsDto = new ProjectsDto();
            compileProcess = new CompileProcess();
            var model = _context.Projects.First(a => a.Id == projectId);
            projectsDto = _mapper.Map<ProjectsDto>(model);
            compileProcess.SaveXML(projectsDto, xmlCode);

        }

        [HttpPost]
        public string loadProject(string projectId)
        {
            ProjectsDto projectsDto = new ProjectsDto();
            compileProcess = new CompileProcess();
            var model = _context.Projects.First(a => a.Id == projectId);
            projectsDto = _mapper.Map<ProjectsDto>(model);
            
            return compileProcess.GetXML(projectsDto);
        }
        

        public void saveArduinoCode([FromBody] arduinoBlockModel arduinoBlockModel)
        {

        }

        private Dictionary<string, string> getProjectPath(ProjectsDto projectsDto)
        {
            string projectPath = "";
            Dictionary<string, string> pathSet = new Dictionary<string, string>();
            UnicodeEncoding uniEncoding = new UnicodeEncoding();

            if (projectsDto.ProjectType == 1)//personal
            {
                projectPath = ProjectFolder + @"Users\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\";

                string projectTemplate = projectPath + projectsDto.Id + ".xml";
                pathSet.Add("xmlPath", projectTemplate);
                string projectSourceCode = projectPath + projectsDto.Id + ".ino";
                pathSet.Add("sourceCodePath", projectSourceCode);
                string projectBuildPath = projectPath + projectsDto.Id + @"BuildPath\";
                pathSet.Add("BuildPath", projectBuildPath);
                string projectSourceOut = projectPath + projectsDto.Id + ".bin";
                pathSet.Add("sourceOutPath", projectSourceOut);

                if (!Directory.Exists(projectPath))
                {
                    Directory.CreateDirectory(projectPath);

                    MemoryStream stream;
                    string writeValue = "";
                    using (stream = new MemoryStream())
                    {
                        var sw = new StreamWriter(stream, uniEncoding);
                        var fileStream = new FileStream(projectTemplate, FileMode.Create, FileAccess.Write);
                        try
                        {
                            sw.Write(writeValue);
                            sw.WriteLine();
                            sw.Flush();//otherwise you are risking empty stream
                            stream.Seek(0, SeekOrigin.Begin);

                            // Test and work with the stream here. 
                            // If you need to start back at the beginning, be sure to Seek again.

                            stream.CopyTo(fileStream);
                        }
                        finally
                        {
                            sw.Dispose();
                            fileStream.Dispose();
                        }
                    }
                }


                return pathSet;
            }
            else if (projectsDto.ProjectType == 2)//group
            {
                projectPath = ProjectFolder + @"Groups\" + projectsDto.GroupId + @"\" + projectsDto.UserId + @"\" + projectsDto.Id + @"\";

                string projectTemplate = projectPath + projectsDto.Id + ".xml";
                pathSet.Add("xmlPath", projectTemplate);
                string projectSourceCode = projectPath + projectsDto.Id + ".ino";
                pathSet.Add("sourceCodePath", projectSourceCode);
                string projectBuildPath = projectPath + projectsDto.Id + @"BuildPath\";
                pathSet.Add("BuildPath", projectBuildPath);
                string projectSourceOut = projectPath + projectsDto.Id + ".bin";
                pathSet.Add("sourceOutPath", projectSourceOut);

                if (!Directory.Exists(projectPath))
                {
                    Directory.CreateDirectory(projectPath);

                    MemoryStream stream;
                    string writeValue = "";
                    using (stream = new MemoryStream())
                    {
                        var sw = new StreamWriter(stream, uniEncoding);
                        var fileStream = new FileStream(projectTemplate, FileMode.Create, FileAccess.Write);
                        try
                        {
                            sw.Write(writeValue);
                            sw.WriteLine();
                            sw.Flush();//otherwise you are risking empty stream
                            stream.Seek(0, SeekOrigin.Begin);

                            // Test and work with the stream here. 
                            // If you need to start back at the beginning, be sure to Seek again.

                            stream.CopyTo(fileStream);
                        }
                        finally
                        {
                            sw.Dispose();
                            fileStream.Dispose();
                        }
                    }
                }


                return pathSet;
            }

            return pathSet;
        }

    }
}